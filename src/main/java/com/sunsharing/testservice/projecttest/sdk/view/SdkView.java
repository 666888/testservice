/*
 * @(#) SdkView
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-22 09:11:21
 */

package com.sunsharing.testservice.projecttest.sdk.view;

public class SdkView {
    private String ip;
    private int port;
    private String requestid;
    private String domain;
    private String wsproxy;
    private String authid;
    private int timeout;
    private String serviceid;

    public SdkView(String ip, int port, String requestid, String domain, String wsproxy, String authid, int timeout, String serviceid) {
        this.ip = ip;
        this.port = port;
        this.requestid = requestid;
        this.domain = domain;
        this.wsproxy = wsproxy;
        this.authid = authid;
        this.timeout = timeout;
        this.serviceid = serviceid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getRequestid() {
        return requestid;
    }

    public void setRequestid(String requestid) {
        this.requestid = requestid;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getWsproxy() {
        return wsproxy;
    }

    public void setWsproxy(String wsproxy) {
        this.wsproxy = wsproxy;
    }

    public String getAuthid() {
        return authid;
    }

    public void setAuthid(String authid) {
        this.authid = authid;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }
}
