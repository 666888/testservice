/*
 * @(#) RpcArraySdkTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-03 14:20:48
 */

package com.sunsharing.testservice.projecttest.sdk.rpcservice;

import com.sunsharing.collagen.data_item.ArrayItem;
import com.sunsharing.collagen.data_item.Item;
import com.sunsharing.collagen.data_item.SimpleItem;
import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.sdk.view.SdkView;
import com.sunsharing.testservice.tools.PoseidonSdkClient;

import java.util.HashMap;
import java.util.Map;

@TestApi
public class RpcArraySdkTest implements TestService {

    private static final String CollaGEN_AP_IP = "192.168.0.131";
    private static final int CollaGEN_AP_PORT = 20184;
    private static final String CollaGEN_Request_ID = "JmApplication";
    private static final String CollaGEN_Domain = "XM.GOV";
    private static final String CollaGEN_WS_Proxy = "FLOW_WEBSERVICE_PROXY";
    private static final String CollaGEN_Authorized_ID = "20190403110727912";
    private static final int CollaGEN_Request_TimeOut = 20;
    private static final String CollaGEN_Service_ID = "XM.GOV.FW.GAJ.CYCRPCARRAYV1";

    @Override
    public String test() {
        SdkView sdkView = new SdkView(CollaGEN_AP_IP, CollaGEN_AP_PORT, CollaGEN_Request_ID,
            CollaGEN_Domain, CollaGEN_WS_Proxy, CollaGEN_Authorized_ID, CollaGEN_Request_TimeOut, CollaGEN_Service_ID);

        Item requireditems = new ArrayItem("RequiredItems", "stringArray");
        Item def = new SimpleItem("CQPEv");
        def.asSimple().setSimpleType(SimpleItem.SimpleType.STRING);
        requireditems.asArray().setDef(def);
        requireditems.asArray().appendStringItem("CQPEv");

        Map<Object,Object> map = new HashMap<Object, Object>();
        map.put("SenderID","jprep");
        map.put("ServiceID","gXrpN");
        map.put("DataObjectCode","vY");
        map.put("Condition","tMwCYSY");

        return PoseidonSdkClient.rpcStructTest(map,requireditems, sdkView);
    }
}

