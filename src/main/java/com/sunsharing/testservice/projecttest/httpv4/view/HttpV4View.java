/*
 * @(#) RequesterView
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-28 10:43:20
 */

package com.sunsharing.testservice.projecttest.httpv4.view;

import com.sunsharing.testservice.tools.SslAccept;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class HttpV4View {

    private String url;
    private String contenttype;
    private String timeout;

    public HttpV4View(String url, String contenttype, String timeout) {
        this.url = url;
        this.contenttype = contenttype;
        this.timeout = timeout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }


    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }



    public HttpURLConnection getHttpURLConnection() throws Exception {
        //ssl证书问题解决

        HttpsURLConnection.setDefaultHostnameVerifier(SslAccept.gethv());
        try {
            URL realUrl = new URL(getUrl());
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", getContenttype());
            connection.setRequestProperty("POSEIDON_TIMEOUT", getTimeout());

            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
