AllTest类发起测试

webtest为通过前端页面发起测试的代码（目前还没用到）

tools为工具包 

projecttest

    -httpv4（用户调用测试代码）
        - getservice（get服务）
        - postservice（post服务）
        - rpcservice（rpc服务）
        - service（普通类型服务）
        - view (pojo类)
        
    -requester（请求方，平台测试代码）
        - getservice（get服务）
        - postservice（post服务）
        - rpcservice（rpc服务）
        - service（普通类型服务）
        - view (pojo类)
            
    -sdk（C3SDK测试代码）
        - getservice（get服务）
        - postservice（post服务）
        - rpcservice（rpc服务）
        - service（普通类型服务）
        - view (pojo类)