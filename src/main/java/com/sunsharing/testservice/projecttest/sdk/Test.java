/*
 * @(#) Test
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-03 13:06:34
 */

package com.sunsharing.testservice.projecttest.sdk;


import com.sunsharing.collagen.data_item.ArrayItem;
import com.sunsharing.collagen.data_item.Item;
import com.sunsharing.collagen.data_item.SimpleItem;
import com.sunsharing.collagen.data_item.StructItem;
import com.sunsharing.collagen.exception.RequestException;
import com.sunsharing.collagen.request.RequestResult;
import com.sunsharing.collagen.request.RequestSetting;
import com.sunsharing.collagen.request.WebServiceProxy;
import java.util.List;

public class Test {
    private static final String CollaGEN_AP_IP = "192.168.0.131";
    private static final int CollaGEN_AP_PORT = 20184;
    private static final String CollaGEN_Request_ID = "JmApplication";
    private static final String CollaGEN_Domain = "XM.GOV";
    private static final String CollaGEN_WS_Proxy = "FLOW_WEBSERVICE_PROXY";
    private static final String CollaGEN_Authorized_ID = "20190412171538314";
    private static final int CollaGEN_Request_TimeOut = 20;
    private static final String CollaGEN_Service_ID = "XM.GOV.FW.GAJ.CYCRESTSIMPLE";

    public static void main(String[] args) {
        RequestSetting.getInstance()
            .setIpAndPort(CollaGEN_AP_IP, CollaGEN_AP_PORT)
            .setRequestId(CollaGEN_Request_ID)
            .setLocalDomain(CollaGEN_Domain)
            .setSyncWsProxyFlowId(CollaGEN_WS_Proxy)
            .setSocketTimeOutInSecond(CollaGEN_Request_TimeOut);

        //创建服务代理对象
        WebServiceProxy wsProxy = new WebServiceProxy(CollaGEN_Service_ID);
        //授权码
        wsProxy.setAuthorizeId(CollaGEN_Authorized_ID);
        wsProxy.appendStringParameter("BODY","{\"param\":\"a\"}");


        StringBuffer result = new StringBuffer();
        try {
            RequestResult ret = wsProxy.request();//发送请求。
            List<Item> retList = ret.getRetItems();//获取返回结果。
            if (retList.size() > 0) {
                for (Item item : retList) {
                    if (item instanceof StructItem) {
                        int count = ((StructItem) item).getCount();
                        for (int i = 0; i < count; i++) {
                            Item secondItem = item.asStruct().getItem(i);
                            if (secondItem instanceof StructItem) {
                                int secondItemcount = ((StructItem) secondItem).getCount();
                                for (int j = 0; j < secondItemcount; j++) {
                                    if (j != secondItemcount - 1) {
                                        result.append(((StructItem) secondItem).getItem(j).asSimple().getValueIfString("") + "+");
                                    } else {
                                        result.append(((StructItem) secondItem).getItem(j).asSimple().getValueIfString(""));
                                    }
                                }
                            }
                            if (secondItem instanceof SimpleItem) {
                                result.append(((SimpleItem) secondItem).getValueIfString("") + "+");
                            }
                            if (secondItem instanceof ArrayItem) {
                                int secondItemcount = ((ArrayItem) secondItem).getCount();
                                for (int j = 0; j < secondItemcount; j++) {
                                    if (j != secondItemcount - 1) {
                                        result.append(((ArrayItem) secondItem).getItem(j).asSimple().getValueIfString("") + "+");
                                    } else {
                                        result.append(((ArrayItem) secondItem).getItem(j).asSimple().getValueIfString(""));
                                    }
                                }
                            }
                        }
                    }
                    if (item instanceof SimpleItem) {
                        result.append(((SimpleItem) item).getValue());
                    }
                }
            }
        } catch (RequestException e) {
            e.printStackTrace();
        }
        System.out.println(result.toString());
    }
}