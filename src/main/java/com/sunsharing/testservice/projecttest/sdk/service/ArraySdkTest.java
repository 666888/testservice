/*
 * @(#) ArraySdkTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-03 11:58:50
 */

package com.sunsharing.testservice.projecttest.sdk.service;

import com.sunsharing.collagen.data_item.ArrayItem;
import com.sunsharing.collagen.data_item.Item;
import com.sunsharing.collagen.data_item.SimpleItem;
import com.sunsharing.collagen.data_item.StructItem;
import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.sdk.view.SdkView;
import com.sunsharing.testservice.tools.PoseidonSdkClient;

@TestApi
public class ArraySdkTest implements TestService {
    private static final String CollaGEN_AP_IP = "192.168.0.131";
    private static final int CollaGEN_AP_PORT = 20184;
    private static final String CollaGEN_Request_ID = "JmApplication";
    private static final String CollaGEN_Domain = "XM.GOV";
    private static final String CollaGEN_WS_Proxy = "FLOW_WEBSERVICE_PROXY";
    private static final String CollaGEN_Authorized_ID = "20190403091342224";
    private static final int CollaGEN_Request_TimeOut = 20;
    private static final String CollaGEN_Service_ID = "XM.GOV.FW.GAJ.CYCARRAY";

    @Override
    public String test() {
        SdkView sdkView = new SdkView(CollaGEN_AP_IP,CollaGEN_AP_PORT,CollaGEN_Request_ID,
            CollaGEN_Domain,CollaGEN_WS_Proxy,CollaGEN_Authorized_ID,CollaGEN_Request_TimeOut,CollaGEN_Service_ID);

        Item msg = new ArrayItem("msg","array");

        Item array_item1 = new ArrayItem("item","array");
        Item struct_item1 = new StructItem("","struct");
        struct_item1.asStruct().appendStringItem("name","vY");
        struct_item1.asStruct().appendStringItem("id","gXrpN");
        Item struct_item2 = new StructItem("","struct");
        struct_item2.asStruct().appendStringItem("name","vY");
        struct_item2.asStruct().appendStringItem("id","gXrpN");

        Item array_item2 = new ArrayItem("item","array");
        Item struct_item3 = new StructItem("","struct");
        struct_item3.asStruct().appendStringItem("name","vY");
        struct_item3.asStruct().appendStringItem("id","gXrpN");
        Item struct_item4 = new StructItem("","struct");
        struct_item4.asStruct().appendStringItem("name","vY");
        struct_item4.asStruct().appendStringItem("id","gXrpN");

        array_item1.asArray().appendItem(struct_item1);
        array_item1.asArray().appendItem(struct_item2);
        array_item2.asArray().appendItem(struct_item3);
        array_item2.asArray().appendItem(struct_item4);

        Item item = new StructItem("item","struct");
        item.asStruct().appendItem(array_item1);
        item.asStruct().appendItem(array_item2);

        msg.asArray().appendItem(item);

        return PoseidonSdkClient.structTest(msg, sdkView);
    }
}
