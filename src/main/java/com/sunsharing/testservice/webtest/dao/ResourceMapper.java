/*
 * @(#) ResourceMapper
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-01 15:03:52
 */

package com.sunsharing.testservice.webtest.dao;

import com.sunsharing.testservice.webtest.entity.ResourceMessage;

import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ResourceMapper {
    @Select(value = "select T2.AUTHORIZATION_CODE,T2.SHORT_AUTHORIZATION_CODE,T3.CODE,T4.CODE as REQUEST_CODE "
        + "from T_RESOURCE T1,T_RESOURCE_APPLY T2,T_COM_NODE T3,T_COM_APPLICATION T4"
        + " where T1.RESOURCE_CODE = #{code}"
        + " AND T1.RESOURCE_ID = T2.RESOURCE_ID"
        + " AND T1.NODE_ID = T3.NODE_ID"
        + " AND T2.APPLICATION_ID = T4.APPLICATION_ID")
    List<ResourceMessage> resourceMessage(String code);
}
