/*
 * @(#) TestSdkTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-22 09:11:21
 */

package com.sunsharing.testservice.projecttest.sdk.service;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.sdk.view.SdkView;
import com.sunsharing.testservice.tools.PoseidonSdkClient;
import java.util.HashMap;
import java.util.Map;

@TestApi
public class SimpleSdkTest implements TestService {
    private static final String CollaGEN_AP_IP = "192.168.0.131"; //*畅享总线接入点IP
    private static final int CollaGEN_AP_PORT = 20184; //*畅享总线接入点端口
    private static final String CollaGEN_Request_ID = "JmApplication"; //畅享总线请求方ID T_Resource_APPLY对应的applicationid然后到T_COM_APPLICATION下对应的code
    private static final String CollaGEN_Domain = "XM.GOV"; //*
    private static final String CollaGEN_WS_Proxy = "FLOW_WEBSERVICE_PROXY"; //*
    private static final String CollaGEN_Authorized_ID = "20190401153146474"; //畅享总线请求授权码 T_Resource_APPLY下的Resource_id与T_Resouce下的 Resource CODE一样对应的AUTH..._CODE
    private static final int CollaGEN_Request_TimeOut = 20; //*
    private static final String CollaGEN_Service_ID = "XM.GOV.FW.GAJ.CYCTESTSERVICE"; //畅享总线外部服务ID T_Resource => Resource CODE

    @Override
    public String test() {
        SdkView sdkView = new SdkView(CollaGEN_AP_IP,CollaGEN_AP_PORT,CollaGEN_Request_ID,
            CollaGEN_Domain,CollaGEN_WS_Proxy,CollaGEN_Authorized_ID,CollaGEN_Request_TimeOut,CollaGEN_Service_ID);

        Map<Object,Object> map = new HashMap<Object, Object>();
        map.put("stringParam","1");
        map.put("intParam",89);
        map.put("boolParam",false);
        map.put("doubleParam",0.0);

        return PoseidonSdkClient.simpleTest(map, sdkView);
    }
}