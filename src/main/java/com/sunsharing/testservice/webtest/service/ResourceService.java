/*
 * @(#) ResourceService
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-01 15:26:06
 */

package com.sunsharing.testservice.webtest.service;

import com.sunsharing.testservice.webtest.dao.ResourceMapper;
import com.sunsharing.testservice.webtest.entity.ResourceMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResourceService {

    @Autowired
    ResourceMapper resourceMapper;

    public List<ResourceMessage> resourceMessage(String code) {
        return resourceMapper.resourceMessage(code);
    }

}
