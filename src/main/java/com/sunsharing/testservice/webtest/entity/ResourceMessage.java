/*
 * @(#) Resource
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-01 14:47:08
 */

package com.sunsharing.testservice.webtest.entity;

public class ResourceMessage {

    private String authorizationcode;
    private String shortauthorizationcode;
    private String code;//服务挂接在哪个节点上
    private String requestcode;//哪个请求方

    public String getAuthorizationcode() {
        return authorizationcode;
    }

    public void setAuthorizationcode(String authorizationcode) {
        this.authorizationcode = authorizationcode;
    }

    public String getShortauthorizationcode() {
        return shortauthorizationcode;
    }

    public void setShortauthorizationcode(String shortauthorizationcode) {
        this.shortauthorizationcode = shortauthorizationcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRequestcode() {
        return requestcode;
    }

    public void setRequestcode(String requestcode) {
        this.requestcode = requestcode;
    }

    @Override
    public String toString() {
        return "ResourceAuth{"
            + "authorizationcode='" + authorizationcode
            + ", shortauthorizationcode='" + shortauthorizationcode
            + ", code='" + code
            + ", requestcode='" + requestcode
            + '}';
    }
}
