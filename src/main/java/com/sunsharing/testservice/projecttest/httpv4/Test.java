/*
 * @(#) Test
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-03 10:31:06
 */

package com.sunsharing.testservice.projecttest.httpv4;

import com.sunsharing.testservice.tools.SslAccept;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class Test {
    private static final String url = "https://192.168.0.131:20184/core/rqEZNn"; //调用的接口+授权短码
    private static final String timeOut = "10";
    private static final String contentType = "application/json";
    private static final String jsonParam = ("{\"simpleInteger\":1,\"simpleDouble\":1.1,\"simpleBoolean\":true,"
        + "\"simpleListString\":\"[\\\"1\\\",\\\"2\\\",\\\"3\\\"]\",\"simpleString\":\"ss\"}");

    public static void main(String[] args) throws Exception {
        HttpsURLConnection.setDefaultHostnameVerifier(SslAccept.gethv());//ssl证书问题解决 工具类见本文的的参考资料
        URL realUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestProperty("POSEIDON_TIMEOUT", timeOut);

            OutputStream outwritestream = connection.getOutputStream();
            outwritestream.write(jsonParam.getBytes());
            outwritestream.flush();
            outwritestream.close();

            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1) {
                bos.write(arr,0,len);
                bos.flush();
            }
            System.out.println(bos.toString("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bos.close();
            connection.disconnect();
        }
    }
}