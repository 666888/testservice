/*
 * @(#) TestService
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-28 14:36:04
 */

package com.sunsharing.testservice.api;

public interface TestService {
    String test() throws Exception;
}
