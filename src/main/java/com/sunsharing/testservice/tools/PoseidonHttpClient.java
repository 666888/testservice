/*
 * @(#) PoseidonHttpClient
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-22 09:11:20
 */

package com.sunsharing.testservice.tools;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

/**
 * Http请求工具
 * */
public class PoseidonHttpClient {

    public static String httpProxyTest(String jsonParam, HttpURLConnection urlConnection) throws Exception {
        HttpURLConnection connection = urlConnection;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Length",String.valueOf(jsonParam.getBytes().length));

            OutputStream outwritestream = connection.getOutputStream();
            outwritestream.write(jsonParam.getBytes());
            outwritestream.flush();
            outwritestream.close();

            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1) {
                bos.write(arr,0,len);
                bos.flush();
            }
            return bos.toString();
        } catch (Exception e) {
            return "Error：" + e.toString();
        } finally {
            bos.close();
            connection.disconnect();
        }
    }


}
