/*
 * @(#) PoseidonSdkClient
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-22 09:11:21
 */

package com.sunsharing.testservice.tools;

import com.sunsharing.collagen.data_item.ArrayItem;
import com.sunsharing.collagen.data_item.Item;
import com.sunsharing.collagen.data_item.SimpleItem;
import com.sunsharing.collagen.data_item.StructItem;
import com.sunsharing.collagen.exception.RequestException;
import com.sunsharing.collagen.request.RequestResult;
import com.sunsharing.collagen.request.RequestSetting;
import com.sunsharing.collagen.request.WebServiceProxy;
import com.sunsharing.testservice.projecttest.sdk.view.SdkView;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public  class PoseidonSdkClient {

    /**
     * 简单类型测试
     * @return 测试结果
     */
    public static String simpleTest(Map<Object, Object> map, SdkView sdkView) {
        RequestSetting.getInstance()
            .setIpAndPort(sdkView.getIp(), sdkView.getPort())
            .setRequestId(sdkView.getRequestid())
            .setLocalDomain(sdkView.getDomain())
            .setSyncWsProxyFlowId(sdkView.getWsproxy())
            .setSocketTimeOutInSecond(sdkView.getTimeout());
        //创建服务代理对象
        WebServiceProxy wsProxy = new WebServiceProxy(sdkView.getServiceid());
        //授权码
        wsProxy.setAuthorizeId(sdkView.getAuthid());

        Iterator<Map.Entry<Object, Object>> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<Object, Object> entry = entries.next();
            wsProxy.appendStringParameter(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
        }

        String result = "";
        try {
            RequestResult ret = wsProxy.request();//发送请求。
            List<Item> retList = ret.getRetItems();//获取返回结果。

            if (retList.size() > 0) {
                Item firstItem = retList.get(0);//获得返回结果中数组中第一个值。
                if (firstItem instanceof SimpleItem) {
                    result = firstItem.asSimple().getValueIfString("");
                }
                if (firstItem instanceof StructItem) {
                    Item secondItem = firstItem.asStruct().getItem(0);
                    result = secondItem.asSimple().getValueIfString("");
                }
            }
        } catch (RequestException e) {
            return "ErrorCode:" + e.getErrorCodeStr()
                + ",ErrorMsg:" + e.getErrorMsg();
        }
        return result;
    }


    /**
     * 结构体类型测试
     * @return 测试结果
     */
    public static String structTest(Item struct_item, SdkView sdkView) {
        RequestSetting.getInstance()
            .setIpAndPort(sdkView.getIp(), sdkView.getPort())
            .setRequestId(sdkView.getRequestid())
            .setLocalDomain(sdkView.getDomain())
            .setSyncWsProxyFlowId(sdkView.getWsproxy())
            .setSocketTimeOutInSecond(sdkView.getTimeout());
        //创建服务代理对象
        WebServiceProxy wsProxy = new WebServiceProxy(sdkView.getServiceid());
        //授权码
        wsProxy.setAuthorizeId(sdkView.getAuthid());
        wsProxy.appendItemParameter(struct_item);
        StringBuffer result = new StringBuffer();
        try {
            RequestResult ret = wsProxy.request();//发送请求。
            List<Item> retList = ret.getRetItems();//获取返回结果。
            if (retList.size() > 0) {
                for (Item item : retList) {
                    if (item instanceof StructItem) {
                        int count = ((StructItem) item).getCount();
                        for (int i = 0; i < count; i++) {
                            Item secondItem = item.asStruct().getItem(i);
                            if (secondItem instanceof StructItem) {
                                int secondItemcount = ((StructItem) secondItem).getCount();
                                for (int j = 0; j < secondItemcount; j++) {
                                    if (j != secondItemcount - 1) {
                                        result.append(((StructItem) secondItem).getItem(j).asSimple().getValueIfString("") + "+");
                                    } else {
                                        result.append(((StructItem) secondItem).getItem(j).asSimple().getValueIfString(""));
                                    }
                                }
                            }
                            if (secondItem instanceof SimpleItem) {
                                result.append(((SimpleItem) secondItem).getValueIfString("") + "+");
                            }
                            if (secondItem instanceof ArrayItem) {
                                int secondItemcount = ((ArrayItem) secondItem).getCount();
                                for (int j = 0; j < secondItemcount; j++) {
                                    if (j != secondItemcount - 1) {
                                        result.append(((ArrayItem) secondItem).getItem(j).asSimple().getValueIfString("") + "+");
                                    } else {
                                        result.append(((ArrayItem) secondItem).getItem(j).asSimple().getValueIfString(""));
                                    }
                                }
                            }
                        }
                    }
                    if (item instanceof SimpleItem) {
                        result.append(((SimpleItem) item).getValue());
                    }
                }
            }
        } catch (RequestException e) {
            return "ErrorCode:" + e.getErrorCodeStr()
                + ",ErrorMsg:" + e.getErrorMsg();
        }
        return result.toString();
    }

    /**
     * RPC的结构体类型测试
     * @return 测试结果
     */
    public static String rpcStructTest(Map<Object, Object> map, Item requireditems, SdkView sdkView) {
        RequestSetting.getInstance()
            .setIpAndPort(sdkView.getIp(), sdkView.getPort())
            .setRequestId(sdkView.getRequestid())
            .setLocalDomain(sdkView.getDomain())
            .setSyncWsProxyFlowId(sdkView.getWsproxy())
            .setSocketTimeOutInSecond(sdkView.getTimeout());
        //创建服务代理对象
        WebServiceProxy wsProxy = new WebServiceProxy(sdkView.getServiceid());
        //授权码
        wsProxy.setAuthorizeId(sdkView.getAuthid());

        Iterator<Map.Entry<Object, Object>> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<Object, Object> entry = entries.next();
            wsProxy.appendStringParameter(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
        }
        wsProxy.appendItemParameter(requireditems);

        String result = "";
        try {
            RequestResult ret = wsProxy.request();//发送请求。
            List<Item> retList = ret.getRetItems();//获取返回结果。

            if (retList.size() > 0) {
                Item firstItem = retList.get(0);//获得返回结果中数组中第一个值。
                if (firstItem instanceof SimpleItem) {
                    result = firstItem.asSimple().getValueIfString("");
                }
                if (firstItem instanceof ArrayItem) {
                    result = ((ArrayItem) firstItem).getItem(0).asSimple().getValueIfString("");
                }
            }
        } catch (RequestException e) {
            return "ErrorCode:" + e.getErrorCodeStr()
                + ",ErrorMsg:" + e.getErrorMsg();
        }
        return result;
    }


}