/*
 * @(#) PostStructJsonRequesterTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-12 14:45:13
 */

package com.sunsharing.testservice.projecttest.requester.postservice;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.requester.view.RequesterView;
import com.sunsharing.testservice.tools.PoseidonHttpClient;

@TestApi
public class PostStructJsonRequesterTest  implements TestService {

    private static final String url = "https://192.168.0.131:20184/core/request/test";
    private static final String contenttype = "application/json";

    @Override
    public String test() throws Exception {
        RequesterView httpProxyView = new RequesterView(url,contenttype);

        String jsonParam = ("{\"branchCenter\":\"ZX_CENTER\",\"nodeId\":\"JM\",\"code\":\"rqEZNn\","
            + "\"params\":{\"simpleInteger\":1,\"simpleDouble\":1.1,\"simpleBoolean\":true,"
            + "\"simpleListString\":\"[\\\"1\\\",\\\"2\\\",\\\"3\\\"]\",\"simpleString\":\"ss\"},\"heads\":{\"POSEIDON_DEBUG\":\"true\"}}");

        return PoseidonHttpClient.httpProxyTest(jsonParam, httpProxyView.getHttpURLConnection());
    }
}
