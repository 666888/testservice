/*
 * @(#) Test
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-03 10:31:53
 */

package com.sunsharing.testservice.projecttest.requester;

import com.sunsharing.testservice.tools.SslAccept;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class Test {
    private static final String url = "https://192.168.0.131:20184/core/request/test"; //调用的接口
    private static final String jsonParam = ("{\"branchCenter\":\"ZX_CENTER\",\"nodeId\":\"JM\",\"code\":\"nUZzAr\","
        + "\"params\":{\"data\":{\"stringParam\":\"YzaRe\",\"intParam\":89,\"boolParam\":false,\"doubleParam\":0.0}},\"heads\":{\"POSEIDON_DEBUG\":\"true\"}}");

    public static void main(String[] args) throws Exception {
        URL realUrl = new URL(url);//调用的接口
        HttpsURLConnection.setDefaultHostnameVerifier(SslAccept.gethv()); //ssl证书问题解决 工具类见本文的的参考资料
        HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Length", String.valueOf(jsonParam.getBytes().length));

            OutputStream outwritestream = connection.getOutputStream();//传入的json参数
            outwritestream.write(jsonParam.getBytes());
            outwritestream.flush();
            outwritestream.close();

            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1) {
                bos.write(arr, 0, len);
                bos.flush();
            }
            System.out.println(bos.toString("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bos.close();
            connection.disconnect();
        }
    }
}