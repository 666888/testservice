/*
 * @(#) ResourceController
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-01 15:25:12
 */

package com.sunsharing.testservice.webtest.controller;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.httpv4.view.HttpV4View;
import com.sunsharing.testservice.projecttest.requester.view.RequesterView;
import com.sunsharing.testservice.tools.PoseidonHttpClient;
import com.sunsharing.testservice.webtest.entity.ResourceMessage;
import com.sunsharing.testservice.webtest.service.ResourceService;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.List;
import java.util.Set;

@RestController
public class ResourceController {

    @Autowired
    ResourceService resourceService;

    /**
     * 管理平台服务测试
     * */
    @RequestMapping(value = "platfromtest",method = RequestMethod.GET)
    public String platFromTest(@RequestParam(name = "code") String code,
                               @RequestParam(name = "jsonParam") String jsonParam,
                               @RequestParam(name = "isRest") String isRest,
                               @RequestParam(name = "isRaw") String isRaw) throws Exception {
        List<ResourceMessage> resourceMessageList = resourceService.resourceMessage(code);
        for (ResourceMessage data : resourceMessageList) {
            if (data.getAuthorizationcode() != null && data.getShortauthorizationcode() != null) {
                String url = "https://192.168.0.130:20184/core/request/test";
                RequesterView httpProxyView = new RequesterView(url,"application/json");
                String json = "";
                if("0".equals(isRest)) {
                    json = "{\"branchCenter\": \"ZX_CENTER\",\"nodeId\": \"JM\",\"code\": \"" + data.getShortauthorizationcode()
                        + "\",\"params\": {\"data\":" + jsonParam + "},\"heads\": {\"POSEIDON_DEBUG\": \"true\"}}";
                } else {
                    if("1".equals(isRaw)) {
                        json = "{\"branchCenter\":\"ZX_CENTER\",\"nodeId\":\"JM\",\"code\":\"" + data.getShortauthorizationcode() + "\",\"serviceType\":\"03\","
                            + "\"restType\":\"raw\",\"params\":" + jsonParam + ""
                            + ",\"heads\":{\"POSEIDON_DEBUG\":\"true\"}}";
                    } else {
                        json = "{\"branchCenter\":\"ZX_CENTER\",\"nodeId\":\"JM\",\"code\":\"" + data.getShortauthorizationcode() + "\",\"serviceType\":\"03\","
                            + "\"params\":" + jsonParam + ""
                            + ",\"heads\":{\"POSEIDON_DEBUG\":\"true\"}}";
                    }
                }
                return PoseidonHttpClient.httpProxyTest(json, httpProxyView.getHttpURLConnection());
            }
        }
        return "无测试结果";
    }

    /**
     * http代理V4 测试
     * */
    @RequestMapping(value = "webhttpv4proxy",method = RequestMethod.GET)
    public String webHttpV4Proxy(@RequestParam(name = "code") String code,
                                 @RequestParam(name = "jsonParam") String jsonParam,
                                 @RequestParam(name = "isRest") String isRest) throws Exception {
        List<ResourceMessage> resourceMessageList = resourceService.resourceMessage(code);
        for (ResourceMessage data : resourceMessageList) {
            if (data.getAuthorizationcode() != null && data.getShortauthorizationcode() != null) {
                String url = "https://192.168.0.131:20184/core/" + data.getShortauthorizationcode();
                HttpV4View httpV4View = new HttpV4View(url,"application/json","20");
                String json = "";
                if("0".equals(isRest)) {
                    json = "{\"data\": " + jsonParam + "}";
                } else {
                    json =  jsonParam;
                }
                return PoseidonHttpClient.httpProxyTest(json, httpV4View.getHttpURLConnection());
            }
        }
        return "无测试结果";
    }

    /**
     * 所有服务一键测试
     * */
    @RequestMapping(value = "alltest",method = RequestMethod.GET)
    public String allTest() throws Exception {
        Reflections reflections = new Reflections("com.sunsharing.testservice.projecttest");
        Set<Class<?>> classesList = reflections.getTypesAnnotatedWith(TestApi.class);
        StringBuffer stringBuffer = new StringBuffer();
        for (Class<?> classes : classesList) {
            TestService testService = (TestService) classes.newInstance();
            int index = classes.getName().lastIndexOf(".") + 1;
            stringBuffer.append(classes.getName().substring(index) + "的测试结果为：" + testService.test() + "\r\n\r\n");
        }
        return stringBuffer.toString();
    }


}