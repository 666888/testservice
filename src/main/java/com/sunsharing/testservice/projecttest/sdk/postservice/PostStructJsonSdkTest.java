/*
 * @(#) PostStructJsonSdkTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-12 15:13:37
 */

package com.sunsharing.testservice.projecttest.sdk.postservice;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.sdk.view.SdkView;
import com.sunsharing.testservice.tools.PoseidonSdkClient;

import java.util.HashMap;
import java.util.Map;

@TestApi
public class PostStructJsonSdkTest implements TestService {

    private static final String CollaGEN_AP_IP = "192.168.0.131";
    private static final int CollaGEN_AP_PORT = 20184;
    private static final String CollaGEN_Request_ID = "JmApplication";
    private static final String CollaGEN_Domain = "XM.GOV";
    private static final String CollaGEN_WS_Proxy = "FLOW_WEBSERVICE_PROXY";
    private static final String CollaGEN_Authorized_ID = "20190412143100138";
    private static final int CollaGEN_Request_TimeOut = 20;
    private static final String CollaGEN_Service_ID = "XM.GOV.FW.GAJ.CYCJSON";

    @Override
    public String test() {
        SdkView sdkView = new SdkView(CollaGEN_AP_IP, CollaGEN_AP_PORT, CollaGEN_Request_ID,
            CollaGEN_Domain, CollaGEN_WS_Proxy, CollaGEN_Authorized_ID, CollaGEN_Request_TimeOut, CollaGEN_Service_ID);

        Map<Object,Object> map = new HashMap<Object, Object>();
        map.put("simpleInteger",1);
        map.put("simpleDouble",1.1);
        map.put("simpleBoolean",true);
        map.put("simpleListString","[\"1\",\"2\",\"3\"]");
        map.put("simpleString","ss");

        return PoseidonSdkClient.simpleTest(map, sdkView);
    }
}

