/*
 * @(#) Sout
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-22 09:11:20
 */

package com.sunsharing.testservice.tools;


public class Sout {
    /**
     * 如果测试结果错误，用err输出
     * */
    public static void sout(String result) {
        if(result.indexOf("Error") > 0 || result.indexOf("错") > 0 ) {
            System.err.println(result);
        } else {
            System.out.println(result);
        }
    }
}
