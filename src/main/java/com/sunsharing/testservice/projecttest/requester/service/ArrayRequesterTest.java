/*
 * @(#) ArrayRequesterTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-03 11:24:12
 */

package com.sunsharing.testservice.projecttest.requester.service;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.requester.view.RequesterView;
import com.sunsharing.testservice.tools.PoseidonHttpClient;

@TestApi
public class ArrayRequesterTest  implements TestService {
    private static final String url = "https://192.168.0.131:20184/core/request/test";
    private static final String contenttype = "application/json";

    @Override
    public String test() throws Exception {
        RequesterView httpProxyView = new RequesterView(url,contenttype);

        String jsonParam = ("{\"branchCenter\":\"ZX_CENTER\",\"nodeId\":\"JM\",\"code\":\"yuYJJ3\","
            + "\"params\":{\"data\":{\"msg\":[{\"item\":[{\"name\":\"vY\",\"id\":\"gXrpN\"},{\"name\":\"vY\",\"id\":\"gXrpN\"}]},{\"item\":[{\"name\":\"vY\",\"id\":\"gXrpN\"},{\"name\":\"vY\",\"id\":\"gXrpN\"}]}]}},\"heads\":{\"POSEIDON_DEBUG\":\"true\"}}");

        return PoseidonHttpClient.httpProxyTest(jsonParam, httpProxyView.getHttpURLConnection());
    }
}
