/*
 * @(#) AllTest
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-03-22 09:11:20
 */

package com.sunsharing.testservice;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.httpv4.getservice.GetHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.postservice.PostArrayHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.postservice.PostSimpleHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.postservice.PostStructJsonHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.postservice.PostStructRawHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.rpcservice.RpcArrayHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.rpcservice.RpcSimpleHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.rpcservice.RpcStructHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.service.ArrayHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.service.StructHttpV4Test;
import com.sunsharing.testservice.projecttest.httpv4.service.SimpleHttpV4Test;
import com.sunsharing.testservice.projecttest.requester.getservice.GetRequesterTest;
import com.sunsharing.testservice.projecttest.requester.postservice.PostArrayRequesterTest;
import com.sunsharing.testservice.projecttest.requester.postservice.PostSimpleRequesterTest;
import com.sunsharing.testservice.projecttest.requester.postservice.PostStructJsonRequesterTest;
import com.sunsharing.testservice.projecttest.requester.postservice.PostStructRawRequesterTest;
import com.sunsharing.testservice.projecttest.requester.rpcservice.RpcArrayRequesterTest;
import com.sunsharing.testservice.projecttest.requester.rpcservice.RpcSimpleRequesterTest;
import com.sunsharing.testservice.projecttest.requester.rpcservice.RpcStructRequesterTest;
import com.sunsharing.testservice.projecttest.requester.service.ArrayRequesterTest;
import com.sunsharing.testservice.projecttest.requester.service.StructRequesterTest;
import com.sunsharing.testservice.projecttest.requester.service.SimpleRequesterTest;
import com.sunsharing.testservice.projecttest.sdk.getservice.GetSdkTest;
import com.sunsharing.testservice.projecttest.sdk.postservice.PostArraySdkTest;
import com.sunsharing.testservice.projecttest.sdk.postservice.PostSimpleSdkTest;
import com.sunsharing.testservice.projecttest.sdk.postservice.PostStructJsonSdkTest;
import com.sunsharing.testservice.projecttest.sdk.postservice.PostStructRawSdkTest;
import com.sunsharing.testservice.projecttest.sdk.rpcservice.RpcArraySdkTest;
import com.sunsharing.testservice.projecttest.sdk.rpcservice.RpcSimpleSdkTest;
import com.sunsharing.testservice.projecttest.sdk.rpcservice.RpcStructSdkTest;
import com.sunsharing.testservice.projecttest.sdk.service.ArraySdkTest;
import com.sunsharing.testservice.projecttest.sdk.service.StructSdkTest;
import com.sunsharing.testservice.projecttest.sdk.service.SimpleSdkTest;
import com.sunsharing.testservice.tools.Sout;

import org.junit.Test;
import org.reflections.Reflections;

import java.util.Set;

public class AllTest {

    public static void main(String[] args) throws Exception {

        SimpleRequesterTest simpleRequesterTest = new SimpleRequesterTest();
        ArrayRequesterTest arrayRequesterTest = new ArrayRequesterTest();
        StructRequesterTest structRequesterTest = new StructRequesterTest();
        RpcSimpleRequesterTest rpcSimpleRequesterTest = new RpcSimpleRequesterTest();
        RpcArrayRequesterTest rpcArrayRequesterTest = new RpcArrayRequesterTest();
        RpcStructRequesterTest rpcStructRequesterTest = new RpcStructRequesterTest();
        PostSimpleRequesterTest postSimpleRequesterTest = new PostSimpleRequesterTest();
        PostArrayRequesterTest postArrayRequesterTest = new PostArrayRequesterTest();
        PostStructJsonRequesterTest postStructJsonRequesterTest = new PostStructJsonRequesterTest();
        PostStructRawRequesterTest postStructRawRequesterTest = new PostStructRawRequesterTest();
        GetRequesterTest getRequesterTest = new GetRequesterTest();

        SimpleHttpV4Test simpleHttpV4Test = new SimpleHttpV4Test();
        ArrayHttpV4Test arrayHttpV4Test = new ArrayHttpV4Test();
        StructHttpV4Test structHttpV4Test = new StructHttpV4Test();
        RpcSimpleHttpV4Test rpcSimpleHttpV4Test = new RpcSimpleHttpV4Test();
        RpcArrayHttpV4Test rpcArrayHttpV4Test = new RpcArrayHttpV4Test();
        RpcStructHttpV4Test rpcStructHttpV4Test = new RpcStructHttpV4Test();
        PostSimpleHttpV4Test postSimpleHttpV4Test = new PostSimpleHttpV4Test();
        PostArrayHttpV4Test postArrayHttpV4Test = new PostArrayHttpV4Test();
        PostStructJsonHttpV4Test postStructJsonHttpV4Test = new PostStructJsonHttpV4Test();
        PostStructRawHttpV4Test postStructRawHttpV4Test = new PostStructRawHttpV4Test();
        GetHttpV4Test getHttpV4Test = new GetHttpV4Test();

        SimpleSdkTest simpleSdkTest = new SimpleSdkTest();
        ArraySdkTest arraySdkTest = new ArraySdkTest();
        StructSdkTest structSdkTest = new StructSdkTest();
        RpcSimpleSdkTest rpcSimpleSdkTest = new RpcSimpleSdkTest();
        RpcArraySdkTest rpcArraySdkTest = new RpcArraySdkTest();
        RpcStructSdkTest rpcStructSdkTest = new RpcStructSdkTest();
        PostSimpleSdkTest postSimpleSdkTest = new PostSimpleSdkTest();
        PostArraySdkTest postArraySdkTest = new PostArraySdkTest(); //SDK调用 暂不支持参数为RAW类型的服务
        PostStructJsonSdkTest postStructJsonSdkTest = new PostStructJsonSdkTest();
        PostStructRawSdkTest postStructRawSdkTest = new PostStructRawSdkTest(); //SDK调用 暂不支持参数为RAW类型的服务
        GetSdkTest getSdkTest = new GetSdkTest();

        Sout.sout("请求方服务测试调用的Simple结果为：" + simpleRequesterTest.test());
        Sout.sout("请求方服务测试调用Array结果为：" + arrayRequesterTest.test());
        Sout.sout("请求方服务测试调用的Struct结果为：" + structRequesterTest.test());
        Sout.sout("请求方服务测试调用的RpcSimple结果为：" + rpcSimpleRequesterTest.test());
        Sout.sout("请求方服务测试调用RpcArray结果为：" + rpcArrayRequesterTest.test());
        Sout.sout("请求方服务测试调用RpcStruct结果为：" + rpcStructRequesterTest.test());
        Sout.sout("请求方服务测试调用PostSimple结果为：" + postSimpleRequesterTest.test());
        Sout.sout("请求方服务测试调用PostArray结果为：" + postArrayRequesterTest.test());
        Sout.sout("请求方服务测试调用Post的StructJson结果为：" + postStructJsonRequesterTest.test());
        Sout.sout("请求方服务测试调用Post的StructRaw结果为：" + postStructRawRequesterTest.test());
        Sout.sout("请求方服务测试调用Get结果为：" + getRequesterTest.test());

        Sout.sout("Http代理V4调用的Simple结果为：" + simpleHttpV4Test.test());
        Sout.sout("Http代理V4调用的Array结果为：" + arrayHttpV4Test.test());
        Sout.sout("Http代理V4调用的Struct结果为：" + structHttpV4Test.test());
        Sout.sout("Http代理V4调用的RpcSimple结果为：" + rpcSimpleHttpV4Test.test());
        Sout.sout("Http代理V4调用的RpcArray结果为：" + rpcArrayHttpV4Test.test());
        Sout.sout("Http代理V4调用的RpcStruct结果为：" + rpcStructHttpV4Test.test());
        Sout.sout("Http代理V4调用的PostSimple结果为：" + postSimpleHttpV4Test.test());
        Sout.sout("Http代理V4调用的PostArray结果为：" + postArrayHttpV4Test.test());
        Sout.sout("Http代理V4调用的Post的StructJson结果为：" + postStructJsonHttpV4Test.test());
        Sout.sout("Http代理V4调用的Post的StructRaw结果为：" + postStructRawHttpV4Test.test());
        Sout.sout("Http代理V4调用的Get结果为：" + getHttpV4Test.test());

        Sout.sout("C3SDK调用的Simple结果为：" +  simpleSdkTest.test());
        Sout.sout("C3SDK调用的Array结果为：" +  arraySdkTest.test());
        Sout.sout("C3SDK调用的Struct结果为：" +  structSdkTest.test());
        Sout.sout("C3SDK调用的RpcSimple结果为：" +  rpcSimpleSdkTest.test());
        Sout.sout("C3SDK调用的RpcArray结果为：" +  rpcArraySdkTest.test());
        Sout.sout("C3SDK调用的RpcStruct结果为：" +  rpcStructSdkTest.test());
        Sout.sout("C3SDK调用的PostSimple结果为：" +  postSimpleSdkTest.test());
        Sout.sout("C3SDK调用的PostArray结果为：" +  postArraySdkTest.test()); //SDK调用 暂不支持参数为RAW类型的服务
        Sout.sout("C3SDK调用的Post的StructJson结果为：" +  postStructJsonSdkTest.test());
        Sout.sout("C3SDK调用的Post的StructRaw结果为：" +  postStructRawSdkTest.test()); //SDK调用 暂不支持参数为RAW类型的服务
        Sout.sout("C3SDK调用的GET结果为：" +  getSdkTest.test());
    }

    /**
     * 通过注解来进行所有服务测试
     * */
    @Test
    public void allTest() throws Exception {
        Reflections reflections = new Reflections("com.sunsharing.testservice.projecttest");
        Set<Class<?>> classesList = reflections.getTypesAnnotatedWith(TestApi.class);
        StringBuffer stringBuffer = new StringBuffer();
        for (Class<?> classes : classesList) {
            TestService testService = (TestService) classes.newInstance();
            int index = classes.getName().lastIndexOf(".") + 1;
            stringBuffer.append(classes.getName().substring(index) + "的测试结果为：" + testService.test() + "\r\n\r\n");
        }
        System.out.println(stringBuffer.toString());
    }
}


