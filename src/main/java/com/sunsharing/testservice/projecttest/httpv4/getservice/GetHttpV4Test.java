/*
 * @(#) GetHttpV4Test
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2019
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author 胡泉水
 * <br> 2019-04-15 10:06:59
 */

package com.sunsharing.testservice.projecttest.httpv4.getservice;

import com.sunsharing.testservice.api.TestApi;
import com.sunsharing.testservice.api.TestService;
import com.sunsharing.testservice.projecttest.httpv4.view.HttpV4View;
import com.sunsharing.testservice.tools.PoseidonHttpClient;

@TestApi
public class GetHttpV4Test  implements TestService {
    private static final String url = "https://192.168.0.131:20184/core/zYrmue";
    private static final String contenttype = "application/json";
    private static final String timeout = "20";

    @Override
    public String test() throws Exception {
        HttpV4View httpV4View = new HttpV4View(url,contenttype,timeout);

        String jsonParam = ("{\"param\":\"a\"}");

        return PoseidonHttpClient.httpProxyTest(jsonParam, httpV4View.getHttpURLConnection());
    }
}
